# GIT FLOW
Có thể dùng thêm git flow để đơn giản hóa các command or dùng git thông thường thì vẫn đầy đủ tính các tính năng.

Mình sẽ trình bày cả hai để các bạn dễ hình dung luồng hoạt động 
## SETUP

OSX - Homebrew
```
brew install git-flow
```
## Luồng hoạt động.

![image](../images/gitFlow/01.svg)

1. Bổ sung nhánh dev từ nhánh mặc định master.

    ```
    git branch -b dev
    git push -u origin dev
    ```

    sử dụng gitflow.

    ```
    git flow init
    ```

2. Các tính năng(feature) sẽ nằm riêng ở từng nhánh. Lấy nhánh `dev` làm nhánh mẹ.

    ![image](../images/gitFlow/02.svg)


    ```
    git checkout dev
    git checkout -b feature_branch
    ```
    ```
    git flow feature start feature_branch
    ```

    Tạo một nhánh để code tính năng mới từ nhanh dev

3. Khi hoàn thình tính năng (feature) của mình, bạn sẽ cần hợp nhất (merge) với nhánh dev

    ```
    git checkout dev
    git merge feature_branch
    ```
    ```
    git flow feature finish feature_branch
    ```

4. Khi dev đã đủ tính năng để phát hành (release)

    ![image](../images/gitFlow/03.svg)

    Để dự án của bạn cũng như quản lý suorce code được clear thì nên tạo ra các version cho những lần phát triển dự án.
    ```
    git checkout dev
    git checkout -b release/0.1.0
    ```

    ```
    git flow release start 0.1.0
    ```

    nhanh release sẽ là nơi cuối cùng chỉnh sửa dể merge vào nhánh master.

    ```
    git checkout master
    git merge release/0.1.0
    ```
    ```
    git flow release finish 0.1.0
    ```

5. Trường hợp phải xử lý code một cách tức thời (hotfix)

    nhánh này sẽ rẽ nhánh trực tiếp từ master
    ```
    git checkout master
    git checkout -b hotfix_branch
    ```
    ```
    git flow hotfix start hotfix_branch
    ```

    ```
    git checkout master
    git merge hotfix_branch
    git checkout dev
    git merge hotfix_branch
    git branch -D hotfix_branch
    ```

    Tương tự với bản release bạn sẽ cần đồng bộ code với master và dev. Khi xong thì xóa nhánh

    ```
    git flow hotfix finish hotfix_branch
    ```

5. Tóm tắt:

    - Nhánh `dev` được tạo từ `master`
    - Nhánh `release` được tạo từ nhánh `dev`
    - Cách nhánh `feature` được tạo ra từ nhánh `dev`
    - Khi một `feature` hoàn thành, nó được sáp nhập vào nhánh `dev`
    - Khi nhánh `release` được thực hiện, nó được hợp nhất vào `dev` và `master`
    - Nếu một vấn đề trong `master` được phát hiện, một nhánh `hotfix` được tạo từ `master`
    - Khi `hotfix` hoàn thành, nó được hợp nhất với cả hai nhánh `dev` và `master`
