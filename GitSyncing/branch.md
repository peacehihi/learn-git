# GIT BRANCH

Các nhánh là một phần của qúa trình phát triển sản phẩm của bạn.

**TIPS** Khi bạn thêm một tính năng mới, bất kể lớn hay nhỏ, bạn nên tạo nên một nhánh để đóng gói cho thay đổi ấy.

## Các lệnh cơ bản cần nắm

- Thông tin chung.
    ```
    git branch
    ```
    Liệt kê danh sách tất cả các nhánh đang có ở `local repo`. tương đương `git  branch --list`

    ```
    git branch -a
    ```
    Liệt kê danh sách tất cả các nhánh đang có ở `local repo` và `remote repo`

    ```
    git branch <name>
    ```

    Tạo một nhánh mới trên `local repo`

    ```
    git branch -u origin/<name>
    git branch --set-upstream-to=origin/<name>
    ```

    Gán cho một nhánh ở `local repo` theo dõi theo một nhánh `remote repo` cố định.
    ```
    git branch -d <name>
    git branch -D <name>
    ```
    Xóa nhánh. Với option `-d` sẽ check các thay đổi chưa được lưu. `-D` bắt buộc xóa.

    ```
    git push origin --delete <branch>
    ```

    Xóa nhánh ở `remote repo`
    git branch -m <name>
    ```
    Đổi tên nhánh hiện tại




