# GIT Syncing

## Git remote
Về cơ bản thì `git remote` giúp bạn quản lý dự án `remote repository`. File cấu hình `.git/config`.

`Repository` của Git gồm 2 loại là: 
- `Remote repository`: Kho này dùng để chia sẻ cho nhiều người và được đặt trên server chuyên dụng.
- `Local repository`: Kho này được đặt trên máy của bạn và chỉ dành cho một người mà thôi.

**Lưu Ý:** 2 định nghĩa trên, mình sẽ sử dụng `remote repo` thay cho nghĩa `kho lưu trữ từ xa` và `local repo` thay cho `kho lữu trữ cá nhân`.

### Các lệnh cơ bản cần nắm:

- Xem cấu hình của dự án

    ```bash
    git remote show <name>
    git remote
    git remote -v
    ```
    Liệt kê các kết nối đến kho lưu trữ bạn đang có (option -v giúp hiển thị thêm thông tin URL)

- Tạo và sửa đổi cấu hình.
Các lệnh sau giúp bạn thay đổi cấu hình lưu trữ ở file `.git/config`.
    
    ```
    git remote add <name> <url>
    ```

    Tạo một kết nối đến `remote repo`. Bạn sẽ dùng `name` để định danh cho `URL` thuận tiện hơn để sử dụng trong cái lênh GIT khác.
    
    ```
    git remote rm <name>
    ```

    Hủy kết nối đến `remote repo`.

    ```
    git remote rename <ole-name> <new-name>
    ```

    Thay đổi tên kết nối của `remote repo`.



